# ge-wine
My personal all in one wine build. two branches: ge-wine and ge-wine-git. git is synced with latest staging upstream on all patch sources (unless something breaks that I can't fix). ge-wine is synced with latest staging release. 

Contains an arch pkgbuild with the following:  

# Staging:  
-staging upstream patches  
-staging ffxiv stutter fix  
-staging CSMT toggle logic fix

# PBA:  
-acommenos pba patches  
-ffxiv heap patch for pba  
Notes: use envar PBA_DISABLE=1 to disable PBA if need. FFXIV does not like the heap sizes that the pba patches specify. To fix this, set __PBA_GEO_HEAP=256 and __PBA_CB_HEAP=64  , or disable pba and use dxvk 

# esync
-new esync patches added 
Note: use envvar WINEESYNC=1 to enable 

# Gallium Nine:  
-gallium 9 upstream patches  

# Game fixes:  
-fallout 4 loading patch and lod patch  
-fallout 4 +skyrim se script extender patch  
-harmony-fix patch  
-steam fix patch  
-path of exile patch fixed WIC error in path of exile dx11 mode.  
-ffxiv stutter patch fix  
-osu audio latency patch

Note: this is only an Arch package with PKGBUILD and files necessary to compile on arch via makepkg. For other distros please see prepare/build sections of PKGBUILD  


# Credits:  
Staging: Wine-Staging team  
PBA: Acommenos and Firerat  
esync: Z. Figura from the Wine-Staging team  
Gallium 9: Gallium 9 team  
CSMT toggle, esync staging compatibility: Tk-Glitch  
Various game patches: WineHQ bug reports  

All of the members of these communities that contribute patches and bug reports.