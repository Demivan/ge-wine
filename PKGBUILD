# Maintainer: Adrià Cereto i Massagué <ssorgatem at gmail.com>
# Contributor: Daniel Bermond < yahoo-com: danielbermond >

pkgname=wine-staging-ge
pkgver=3.15.r94.gbfe8510ec0.staging.esync.nine
pkgrel=1
tag=3.14
esync='esync'
esyncversion='esynca7faa85'
pkgdesc='A compatibility layer for running Windows programs (staging branch, git version) with Vulkan patches'
arch=('i686' 'x86_64')
url='https://www.wine-staging.com/'
license=('LGPL')
_depends=(
    'attr'                  'lib32-attr'
    'fontconfig'            'lib32-fontconfig'
    'lcms2'                 'lib32-lcms2'
    'libxml2'               'lib32-libxml2'
    'libxcursor'            'lib32-libxcursor'
    'libxrandr'             'lib32-libxrandr'
    'libxdamage'            'lib32-libxdamage'
    'libxi'                 'lib32-libxi'
    'gettext'               'lib32-gettext'
    'freetype2'             'lib32-freetype2'
    'glu'                   'lib32-glu'
    'libsm'                 'lib32-libsm'
    'gcc-libs'              'lib32-gcc-libs'
    'libpcap'               'lib32-libpcap'
    'desktop-file-utils'
)
makedepends=('git' 'autoconf' 'ncurses' 'bison' 'perl' 'fontforge' 'flex'
    'gcc>=4.5.0-2'
    'giflib'                'lib32-giflib'
    'libpng'                'lib32-libpng'
    'gnutls'                'lib32-gnutls'
    'libxinerama'           'lib32-libxinerama'
    'libxcomposite'         'lib32-libxcomposite'
    'libxmu'                'lib32-libxmu'
    'libxxf86vm'            'lib32-libxxf86vm'
    'libldap'               'lib32-libldap'
    'mpg123'                'lib32-mpg123'
    'openal'                'lib32-openal'
    'v4l-utils'             'lib32-v4l-utils'
    'alsa-lib'              'lib32-alsa-lib'
    'libxcomposite'         'lib32-libxcomposite'
    'mesa'                  'lib32-mesa'
    'libgl'                 'lib32-libgl'
    'opencl-icd-loader'     'lib32-opencl-icd-loader'
    'libxslt'               'lib32-libxslt'
    'libpulse'              'lib32-libpulse'
    'libva'                 'lib32-libva'
    'gtk3'                  'lib32-gtk3'
    'gst-plugins-base-libs' 'lib32-gst-plugins-base-libs'
    'vulkan-icd-loader'     'lib32-vulkan-icd-loader'
    'sdl2'                  'lib32-sdl2'
    'samba'
    'opencl-headers'
)
optdepends=(
    'giflib'                'lib32-giflib'
    'libpng'                'lib32-libpng'
    'libldap'               'lib32-libldap'
    'gnutls'                'lib32-gnutls'
    'mpg123'                'lib32-mpg123'
    'openal'                'lib32-openal'
    'v4l-utils'             'lib32-v4l-utils'
    'libpulse'              'lib32-libpulse'
    'alsa-plugins'          'lib32-alsa-plugins'
    'alsa-lib'              'lib32-alsa-lib'
    'libjpeg-turbo'         'lib32-libjpeg-turbo'
    'libxcomposite'         'lib32-libxcomposite'
    'libxinerama'           'lib32-libxinerama'
    'ncurses'               'lib32-ncurses'
    'opencl-icd-loader'     'lib32-opencl-icd-loader'
    'libxslt'               'lib32-libxslt'
    'libva'                 'lib32-libva'
    'gtk3'                  'lib32-gtk3'
    'gst-plugins-base-libs' 'lib32-gst-plugins-base-libs'
    'vulkan-icd-loader'     'lib32-vulkan-icd-loader'
    'sdl2'                  'lib32-sdl2'
    'cups'
    'samba'
    'dosbox'
)
options=('staticlibs')
install="$pkgname".install
source=('wine-git'::'git+https://github.com/wine-mirror/wine.git'
        "wine-staging"::'git+https://github.com/wine-staging/wine-staging.git'
        'wine-pba'::'git+https://github.com/firerat/wine-pba.git'
        https://github.com/zfigura/wine/releases/download/${esyncversion}/esync.tgz
        'gallium9'::'git+https://github.com/sarnex/wine-d3d9-patches.git'
        'fallout4.patch'
        'pathofexile.patch'
        'f4skyrimse-fix.patch'
        'steam.patch'
        'CSMT-toggle.patch'
        'esync-staging-fixes.patch'
        'esync-no_alloc_handle.patch'
        'osu-low-latency-audio.patch'
        'harmony-fix.diff'
        '30-win32-aliases.conf'
        'wine-binfmt.conf')
sha256sums=('SKIP'
            'SKIP'
            'SKIP'
            'SKIP'            
            #'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            '50ccb5bd2067e5d2739c5f7abcef11ef096aa246f5ceea11d2c3b508fc7f77a1'
            '9901a5ee619f24662b241672a7358364617227937d5f6d3126f70528ee5111e7'
            '6dfdefec305024ca11f35ad7536565f5551f09119dda2028f194aee8f77077a4')

provides=(
	"wine=$pkgver"
	"wine-wow64=$pkgver"
	"wine-staging=$pkgver"
	"wine-esync=$pkgver"
)

conflicts=('wine' 'wine-wow64' 'wine-staging' 'wine-esync')
makedepends=(${makedepends[@]} ${depends[@]})

prepare() {
    cd "${srcdir}"/wine-staging
    git reset --hard HEAD      # restore tracked files
    git clean -xdf             # delete untracked files
    #this is temporarily commented out until next release due to league of legends being fixed between 3.13.1 and 3.14
    #git checkout tags/v${tag}.1     # version checkout
    #git checkout ec47c04ab3768d65b72de4331c3e17742054eaf0
    
    cd "${srcdir}"/gallium9
    git reset --hard HEAD      # restore tracked files
    git clean -xdf             # delete untracked files
    #git checkout tags/wine-d3d9-${tag}     # version checkout
    git checkout tags/wine-d3d9-3.13     # version checkout

    cd "${srcdir}"/wine-pba
    git reset --hard HEAD      # restore tracked files
    git clean -xdf             # delete untracked files
    git checkout tags/knobs_and_switches-v${tag}    # version checkout

    cd "${srcdir}"/wine-git
    # restore the wine tree to its git origin state, without wine-staging patches
    # (necessary for reapllying wine-staging patches in succedent builds,
    # otherwise the patches will fail to be reapplied)
    msg2 'Cleaning wine source code tree...'
    git reset --hard HEAD      # restore tracked files
    git clean -xdf             # delete untracked files
    git checkout "$(../wine-staging/patches/patchinstall.sh --upstream-commit)"     # version checkout
        
    # freetype harmony fix
    echo "***freetype harmony fix***"
    patch -Np1 < ../'harmony-fix.diff'
    
    # fix steam
    echo "***steam fix***"
    patch -Np1 < ../'steam.patch'
    
    # fix fallout 4
    echo "***fallout 4 fix***"
    patch -Np1 < ../'fallout4.patch'
    
    # fix fallout4/skyrim script extender
    echo "***fallout 4/scyrim script extender fix***"
    patch -Np1 < ../'f4skyrimse-fix.patch'

    # fix path of exile
    echo "***path of exile fix***"
    patch -Np1 < ../'pathofexile.patch'
    
    # osu low latency audio patch
    echo "***OSU low latency audio patch***"
    patch -Np1 < ../'osu-low-latency-audio.patch'

    cd "${srcdir}"/wine-staging
    # fix csmt
    echo "***CSMT toggle fix***"
    patch -Np1 < ../'CSMT-toggle.patch'
    
    # ffxiv staging stutter fix - only use for ffxiv, can cause input stutter/freeze in other games
    #ffxiv_mouse_fix="-W server-send_hardware_message"
    
    #as of wine staging 3.12 there is an issue with pulseaudio, temporarily disable the staging patches for it
    staging_pulseaudio_exclusion="-W winepulse-PulseAudio_Support"
    
    #esync staging exclusions
    esync_exclusions="-W msvfw32-ICGetDisplayFormat"
    
    cd "${srcdir}"/wine-git

    # staging patches
    echo "***staging patches***"
    ../wine-staging/patches/patchinstall.sh --all ${esync_exclusions} ${staging_pulseaudio_exclusion} ${ffxiv_mouse_fix}

    # esync patches
    echo "***esync patches***"
    cd "${srcdir}"/esync
    patch -Np1 < ../'esync-staging-fixes.patch'

    cd ${srcdir}/wine-git
    for _f in "${srcdir}"/esync/*.patch; do
	git apply -C1 --verbose < ${_f}
    done

    echo "***esync noalloc handle patch***"
    patch -Np1 < ../'esync-no_alloc_handle.patch'

    cd ${srcdir}/wine-git
    echo "***pba patches***"
    for _f in ../wine-pba/patches/*.patch; do
        echo "applying ${_f##*/}"
        patch -Np1 <  "${_f}"
    done

    # gallium 9 patches
    echo "starting g9 staging helper"
    patch -Np1 < ../gallium9/'staging-helper.patch'

    echo "*** g9 patches***"
    patch -Np1 < ../gallium9/'wine-d3d9.patch'

    autoreconf -f
    
    # fix path of opencl headers
    sed 's|OpenCL/opencl.h|CL/opencl.h|g' -i configure*
    
    # delete old build dirs (from previous builds) and make new ones
    rm	-rf "${srcdir}"/"${pkgname}"-{32,64}-build
    mkdir -p "${srcdir}"/"${pkgname}"-64-build
    mkdir -p "${srcdir}"/"${pkgname}"-32-build

}

pkgver() {
    cd "${srcdir}"/wine-staging
	local _stagingTag="$(git tag --sort='version:refname' | tail -n1 | sed 's/-/./g;s/^v//;s/\.rc/rc/')"
	local _stagingVer="$(git describe --long --tags \
								| sed 's/\([^-]*-g\)/r\1/;s/-/./g;s/^v//;s/\.rc/rc/' \
								| sed "s/^latest.release/${_stagingTag}/")"
    cd "${srcdir}"/wine-git
	local _wineVer="$(git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g;s/^v//;s/\.rc/rc/')"
	

	# retrieve current wine-pba version
	#cd "${srcdir}"/wine-pba
	#local _pbaTag="$(git tag --sort='version:refname' | tail -n1 | sed 's/-/./g;s/^v//;s/\.rc/rc/')"
    #local _pbaVer=$( printf "%s.r%s.%s" "$_pbaTag" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)" )

	 stg=".staging"
	 zf=".esync"
	 #firerat=".pba"
	 nine=".nine"

	# version string
    printf '%s%s%s%s%s' "${_wineVer#wine.}" "$stg" "$zf" "$firerat" "$nine"
}

build() {    
    # delete old build dirs (from previous builds) and make new ones
    rm    -rf wine-staging-{32,64}-build
    mkdir -p  wine-staging-32-build

    # workaround for FS#55128
    # https://bugs.archlinux.org/task/55128
    # https://bugs.winehq.org/show_bug.cgi?id=43530
    export CFLAGS="${CFLAGS/-fno-plt/}"
    export LDFLAGS="${LDFLAGS/,-z,now/}"
    
    # build wine-staging 64-bit
    # (according to the wine wiki, this 64-bit/32-bit building order is mandatory)
    if [ "$CARCH" = "x86_64" ] 
    then
        msg2 'Building Wine-64...'
        mkdir -p wine-staging-64-build
        cd "${srcdir}"/wine-staging-64-build
        ../wine-git/configure \
                        --prefix='/usr' \
                        --libdir='/usr/lib' \
                        --with-x \
                        --with-gstreamer \
                        --enable-win64 \
                        --with-xattr
        make
        local _wine32opts=(
                    '--libdir=/usr/lib32'
                    "--with-wine64=${srcdir}/wine-staging-64-build"
        )
        export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'
    fi
    
    # build wine-staging 32-bit
    msg2 'Building Wine-32...'
    cd "${srcdir}"/wine-staging-32-build
    ../wine-git/configure \
                    --prefix='/usr' \
                    --with-x \
                    --with-gstreamer \
                    --with-xattr \
                    "${_wine32opts[@]}"
    make
}

package() {
    depends=("${_depends[@]}")
    
    # package wine-staging 32-bit
    # (according to the wine wiki, this reverse 32-bit/64-bit packaging order is important)
    msg2 'Packaging Wine-32...'
    cd "${srcdir}"/wine-staging-32-build
    
    if [ "$CARCH" = 'i686' ] 
    then
        make prefix="${pkgdir}/usr" install
    else
        make prefix="${pkgdir}/usr" \
             libdir="${pkgdir}/usr/lib32" \
             dlldir="${pkgdir}/usr/lib32/wine" install
        
        # package wine-staging 64-bit
        msg2 'Packaging Wine-64...'
        cd "${srcdir}"/wine-staging-64-build
        make prefix="${pkgdir}/usr" \
             libdir="${pkgdir}/usr/lib" \
             dlldir="${pkgdir}/usr/lib/wine" install
    fi
    
    # font aliasing settings for Win32 applications
    install -d "$pkgdir"/etc/fonts/conf.{avail,d}
    install -m644 "${srcdir}/30-win32-aliases.conf" "${pkgdir}/etc/fonts/conf.avail"
    ln -s ../conf.avail/30-win32-aliases.conf       "${pkgdir}/etc/fonts/conf.d/30-win32-aliases.conf"
    
    # wine binfmt
    install -D -m644 "${srcdir}/wine-binfmt.conf"   "${pkgdir}/usr/lib/binfmt.d/wine.conf"
}
